import { app, BrowserWindow } from 'electron'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 1080,
    // useContentSize: true,  // Boolean (可选) - width 和 height 将使用 web 页面的尺寸, 这意味着实际窗口的尺寸应该包括窗口框架的大小，稍微会大一点。 默认值为 false.
    width: 1920,
    webPreferences: {webSecurity: false,allowDisplayingInsecureContent:true,allowRunningInsecureContent:true},
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    // alert('窗口要关闭啦mainWindow');
    mainWindow = null
  })

  mainWindow.onbeforeunload = function(e) {
    console.log('I do not want to be closed');

    // Unlike usual browsers, in which a string should be returned and the user is
    // prompted to confirm the page unload, Electron gives developers more options.
    // Returning empty string or false would prevent the unloading now.
    // You can also use the dialog API to let the user confirm closing the application.
    e.returnValue = false;
  };
  
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */




//  自己写的
// const { app, autoUpdater, electron } = require('electron')
// const electron = require('electron');
// 控制应用生命周期的模块
// const app = electron.app;
// 创建本地浏览器窗口的模块
// const BrowserWindow = electron.BrowserWindow;

// 指向窗口对象的一个全局引用，如果没有这个引用，那么当该javascript对象被垃圾回收的
// 时候该窗口将会自动关闭
// let win;

// function createWindow() {
  
//   // 创建一个新的浏览器窗口
//   win = new BrowserWindow({width: 1920, height: 1080});

//   // 并且装载应用的index.html页面,__dirname为当前文件路径
//   mainWindow.loadURL(url.format({
//     pathname: path.join(__dirname, "./views/login.vue"),
//     protocol: 'file:',
//     slashes: true
// }));

  // 打开开发工具页面
//   win.webContents.openDevTools();

  // 当窗口已经关闭时调用的方法
  // win.on('closed', function() {
  //   alert('窗口要关闭啦win');
  //   // 解除窗口对象的引用，通常而言如果应用支持多个窗口的话，你会在一个数组里
  //   // 存放窗口对象，在窗口关闭的时候应当删除相应的元素。
  //   win = null;
  // });
  
// }

// 当Electron完成初始化并且已经创建了浏览器窗口，则该方法将会被调用。
// 有些API只能在该事件发生后才能被使用。
// app.on('ready', createWindow);

// const autoUpdater = require('auto-updater');
// const appVersion = require('./package.json').version;
// const os = require('os').platform();
// app.on('ready', function(){
//   // if(process.argv[1] == '--squirrel-firstrun'){
//   //   createWindow();
//   //   return;    
//   // }
//   createWindow();

// });

//  当窗口准备关闭时调用的方法
// app.on('closed', function() {
//   alert('窗口要关闭啦');
// });

// 当所有的窗口被关闭后退出应用
// app.on('window-all-closed', function() {
//   // 对于OS X系统，应用和相应的菜单栏会一直激活直到用户通过Cmd + Q显式退出
//   if (process.platform !== 'darwin') {
//     app.quit();
//   }
// });

// app.on('activate', function() {
//   // 对于OS X系统，当dock图标被点击后会重新创建一个app窗口，并且不会有其他
//   // 窗口打开
//   if (win === null) {
//     createWindow();
//   }
// });
