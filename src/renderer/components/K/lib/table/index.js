import KTable from "./table.vue"

KTable.install  = function(Vue) {
  Vue.component(KTable.name, KTable);
};

export default KTable;