import KButton from "./button.vue"

KButton.install  = function(Vue) {
  Vue.component(KButton.name, KButton);
};

export default KButton;