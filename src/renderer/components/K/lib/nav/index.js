import KNav from './nav'

KNav.install = (Vue) => {
    Vue.component(KNav.name,KNav)
}

export default KNav;