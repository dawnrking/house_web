import button from './lib/button'
import table from './lib/table'
import nav from './lib/nav'
import input from './lib/input'

const kcomponets = [button,table,nav,input]

kcomponets.install = (Vue) =>{
    kcomponets.map((val)=>{
        Vue.component(val.name,val)
    })
}

export default kcomponets