import editresources from './lib/editresources'  //添加修改客源
import editroom from './lib/editroom'  //添加修改房源
import editbuildingdictionary from './lib/editbuildingdictionary'  //楼盘字典
import editfollow from './lib/editfollow'  //房客源跟进
import permission from './lib/permission'   //权限
import chooselist from './lib/chooselist'   //选择房客源信息

const mcomponets = [editresources,editroom,editbuildingdictionary,editfollow,permission,chooselist]

mcomponets.install = (Vue) =>{
    mcomponets.map((val)=>{
        Vue.component(val.name,val)
    })
}

export default mcomponets