import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
      routes: [
    // {
    //   path: '/',
    //   component: require('@/components/LandingPage').default
    // },
    // {
    //   path: '*',
    //   redirect: '/'
  // }
            {
                  path: '*',
                  redirect: '/'
            },
                  {
                  path: '/',
                  name: 'login',
                  component: resolve => require(['../views/login'], resolve).default,
            },  
                  {
                  path: '/home',
                  name: 'home',
                  component: resolve => require(['../views/home'], resolve),
                  children:[
                        {
                              path: '/resources',/*客源 */
                              name: 'resources',
                              component: resolve => require(['../views/home/resources/resources'], resolve),
                        },
                        {
                              path: '/addresource',/*新增客源 */
                              name: 'addresource',
                              component: resolve => require(['../views/home/resources/addresource'], resolve),
                        },
                        {
                              path: '/resourcefollow',/*客源跟进列表 */
                              name: 'resourcefollow',
                              component: resolve => require(['../views/home/resources/resourcefollow'], resolve),
                        },
                        {
                              path: '/room',/*房源 */
                              name: 'room',
                              component: resolve => require(['../views/home/room/room'], resolve),
                        },
                        {
                              path: '/addroom',  /* 添加房源 */
                              name: 'addroom',
                              component: resolve => require(['../views/home/room/addroom'],resolve),
                        },
                        {
                              path: '/roomfollow',/*房源跟进列表 */
                              name: 'roomfollow',
                              component: resolve => require(['../views/home/room/roomfollow'], resolve),
                        },
                        {
                              path: '/department',/*部门 */
                              name: 'department',
                              component: resolve => require(['../views/home/department'], resolve),
                        },
                        {
                              path: '/employee',/*员工 */
                              name: 'employee',
                              component: resolve => require(['../views/home/employee'], resolve),
                        },
                        {
                              path: '/duties',/*职务 */
                              name: 'duties',
                              component: resolve => require(['../views/home/duties'], resolve),
                        },
                        {
                              path: '/rankings',/*打卡 */
                              name: 'rankings',
                              component: resolve => require(['../views/home/rankings'], resolve),
                        },
                        {
                              path: '/contract',/*合同 */
                              name: 'contract',
                              component: resolve => require(['../views/home/contract'], resolve),
                        },
                        {
                              path: '/reclaimresources',/*客源回收站*/
                              name: 'reclaimresources',
                              component: resolve => require(['../views/home/reclaim/reclaimresources'], resolve),
                        },
                        {
                              path: '/reclaimhourse',/*房源回收站*/
                              name: 'reclaimhourse',
                              component: resolve => require(['../views/home/reclaim/reclaimhourse'], resolve),
                        },
                        {
                              path: '/reclaimbuildingdictionary',/*楼盘字典回收站*/
                              name: 'reclaimbuildingdictionary',
                              component: resolve => require(['../views/home/reclaim/reclaimbuildingdictionary'], resolve),
                        },
                        {
                              path: '/reclaimdepartment',/*部门回收站*/
                              name: 'reclaimdepartment',
                              component: resolve => require(['../views/home/reclaim/reclaimdepartment'], resolve),
                        },
                        {
                              path: '/reclaimstaff',/*员工回收站*/
                              name: 'reclaimstaff',
                              component: resolve => require(['../views/home/reclaim/reclaimstaff'], resolve),
                        },
                        {
                              path: '/reclaimsummary',/*工作总结回收站*/
                              name: 'reclaimsummary',
                              component: resolve => require(['../views/home/reclaim/reclaimsummary'], resolve),
                        },
                        {
                              path: '/achievementranking',/*排行榜    业绩排行*/
                              name: 'achievementranking',
                              component: resolve => require(['../views/home/ranking/achievementranking'], resolve),
                        },
                        {
                              path: '/entryroomranking',/*排行榜     录入房源排行*/
                              name: 'entryroomranking',
                              component: resolve => require(['../views/home/ranking/entryroomranking'], resolve),
                        },
                        {
                              path: '/entryresourcesranking',/*排行榜      录入客源排行*/
                              name: 'entryresourcesranking',
                              component: resolve => require(['../views/home/ranking/entryresourcesranking'], resolve),
                        },
                        {
                              path: '/roomfollowranking',/*排行榜         房源跟进排行*/
                              name: 'roomfollowranking',
                              component: resolve => require(['../views/home/ranking/roomfollowranking'], resolve),
                        },
                        {
                              path: '/resourcesfollowranking',/*排行榜       客源跟进排行*/
                              name: 'resourcesfollowranking',
                              component: resolve => require(['../views/home/ranking/resourcesfollowranking'], resolve),
                        },
                        {
                              path: '/earnestmoney',/*诚意金管理 */
                              name: 'earnestmoney',
                              component: resolve => require(['../views/home/earnestmoney'], resolve),
                        },
                        {
                              path: '/worksummary',/*工作总结管理*/
                              name: 'worksummary',
                              component: resolve => require(['../views/home/worksummary'], resolve),
                        },
                        {
                              path: '/buildingdictionary',/*楼盘字典 */
                              name: 'buildingdictionary',
                              component: resolve => require(['../views/home/buildingdictionary'], resolve),
                        },
                        {
                              path: '/log',/*日志 */
                              name: 'log',
                              component: resolve => require(['../views/home/log'], resolve),
                        },
                        {
                              path: '/automatictransferrule',/*自动转移房客规则 */
                              name: 'automatictransferrule',
                              component: resolve => require(['../views/home/transferrule/automatictransferrule'], resolve),
                        },
                        {
                              path: '/stafftransfertenant',/*员工转房客 */
                              name: 'stafftransfertenant',
                              component: resolve => require(['../views/home/transferrule/stafftransfertenant'], resolve),
                        },
                        {
                              path: '/publictransferprivate',/*私盘转公盘 */
                              name: 'publictransferprivate',
                              component: resolve => require(['../views/home/transferrule/publictransferprivate'], resolve),
                        },
                  ]
            }
      ]
})
