// if(require('electron-squirrel-startup')) return;

import Vue from 'vue'

import App from './App'
import router from './router'
import store from './store'


// import KUI from './components/k'
import Multiplex from './components/Multiplex'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import iconFont from './style/iconfont.css';  //阿里iconfont

// vue的提示
import Vtip from 'vtip'
import 'vtip/lib/index.min.css'
// 注册指令使用
Vue.use(Vtip.directive)
// 工具函数调用
Vue.prototype.$tip = Vtip.tip;


Vue.use(ElementUI);
Vue.use(iconFont);

Vue.use(Multiplex);

import base from './config/functions.js'     //公共方法
import { getMacOsVersion } from 'builder-util/out/macosVersion';
Vue.use(base);

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
// Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false;


/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')






