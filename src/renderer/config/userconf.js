const baseurl = "http://fangchan.dingzhou4.com/";
import axios from 'axios'
import Qs from 'qs'
import permconfig from './permconfig.js'
/*默认设置*/
const instance = axios.create({
    baseURL:baseurl,
    headers: {'X-Custom-Header': 'foobar'},
    
})

let setToken = (data,calback)=> {
    if (data) {      
        data['token'] = window.localStorage.token;
        return data;
    }else {
        return {
            token:window.localStorage.token
        }
    }
       
}
/*登录 */
const login = (data,calback) => {
        return instance.post("login",data,{
            paramsSerializer: function(data) {
                return Qs.stringify(data)
            },
        })
}

/*客源 */
const resources = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("customers/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("customers/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*客源  回收（软删除）*/
    reclaim: (data,calback) => {
        return instance.post("customers/recyclebin",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("customers/update",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*跟进列表*/
    follow: (data,calback) => {
        return instance.get("customers/follow",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 添加跟进
    addfollow: (data,calback) => {
        return instance.post("customers/addfollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 修改跟进
    editfollow: (data,calback) => {
        return instance.post("customers/updatefollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 客源所有跟进列表
    getallfollowlist: (data,calback) => {
        return instance.get("customers/followlists",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //删除客源跟进
    delfollow: (data,calback) => {
        return instance.post("customers/delfollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //验证手机号
    validateTel: (data,calback) => {
        return instance.get("customers/repeat",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 下拉列表
    optlist: (data,calback) => {
        return instance.get("customers/getdefaulttype",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //客源列表查询条件下拉数据
    searchoptlist: (data,calback) => {
        return instance.get("customers/getlisttype",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*获取与该客源相匹配的房源*/
    roominfo: (data,calback) => {
        return instance.post("customers/recommend",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //客源修改状态、业绩分配
    addachievement: (data,calback) => {
        return instance.post("customers/addcustomersachievement",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

/*分类*/
const types = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("account/typelist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("account/typeadd",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.post("account/typedel",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("account/typeedit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

/*房源 */
const room = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("room/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("room/inserthouse",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*回收*/
    reclaim: (data,calback) => {
        return instance.post("room/softdel",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("room/updatahouse",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*跟进*/
    follow: (data,calback) => {
        return instance.post("room/roomfollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*修改跟进*/
    editfollow: (data,calback) => {
        return instance.post("room/updatefollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 用途
    type: (data,calback) => {
        return instance.get("customers/getalltypedetail",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //楼盘字典
    building: (data,calback) => {
        return instance.post("wordbook/listwordbook",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*简易房源 列表*/
    easylist: (data,calback) => {
        return instance.get("room/fastlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*新增页面加载的所有类型*/
    optlist: (data,calback) => {
        return instance.get("room/getdefaulttype",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*添加房源之前根据、区code、楼盘字典ID 栋座内容 位置内容 房号 查询该房源是不是重复录入*/
    repeat: (data,calback) => {
        return instance.get("room/checkhouse",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*列表页  查询条件的下拉数据*/
    searchoptlist: (data,calback) => {
        return instance.get("room/getlisttype",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*获取与该房源相匹配的客源信息*/
    customerinfo: (data,calback) => {
        return instance.get("room/matchcustomerinfo",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 获取房源跟进列表
    getfollowlist: (data,calback) => {
        return instance.get("room/getfollow",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 房源所有跟进列表
    getallfollowlist: (data,calback) => {
        return instance.get("room/getallfollow",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //删除房源跟进
    delfollow: (data,calback) => {
        return instance.post("room/delfollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //房源修改状态、业绩分配
    addachievement: (data,calback) => {
        return instance.post("room/addachievement",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //验证业主手机号
    validateTel: (data,calback) => {
        return instance.get("room/verificationphone",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 房客跟进id 获取跟进与提醒
    roomcustomersfollow: (data,calback) => {
        return instance.post("user/roomcustomersfollow",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //房客跟进提醒    我知道了  数据
    dealdata: (data,calback) => {
        return instance.get("room/roomcustomerdefaultdata",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //业务处理 (处理跟进提醒到【我知道了】)
    businessprocessing: (data,calback) => {
        return instance.post("user/businessprocessing",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 房源图片列表
    imageslists: (data,calback) => {
        return instance.get("room/imageslists",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 房源上传图片
    roomuploadimg: (data,calback) => {
        return instance.post("room/uploadimg",data).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 房源删除图片（单图）
    delimg: (data,calback) => {
        return instance.post("room/delimg",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    }
}

/*打卡    房客源业务提醒*/
const signin = {
    /*上班打卡*/
    begin: (data,calback) => {
        return instance.get("signin/gotowork",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*下班打卡*/
    finish: (data,calback) => {
        return instance.get("signin/afterwork",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*员工签到列表 */
    rankings: (data,calback) => {
        return instance.get("signin/rankings",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },

    /*房客源业务提醒 */
    remind: (data,calback) => {
        return instance.get("user/businessreminder",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
   
}

/*部门 */
const department = {
    // 查看系统管理组
    look: (data,calback) => {
        return instance.get("department/info",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*列表*/
    list: (data,calback) => {
        return instance.get("department/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("department/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.post("department/del",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("department/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //获取登录账号属于哪个中介公司的部门下拉框
    nameOpt_bak: (data,calback) => {
        return instance.get("department/dropdown",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //根据中介公司的部门ID 获取该部门下面所有员工的下拉框
    userOpt: (data,calback) => {
        return instance.get("department/userdropdown",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*修改部门权限*/
    editpermission: (data,calback) => {
        return instance.post("department/editdepartmentpermission",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 批量修改该部门下面所有员工的权限
    editdepartmentdownuserpermission: (data,calback) => {
        return instance.get("department/editdepartmentdownuserpermission",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },

    nameOpt: (data,calback, qx_key = '') => {
        var param = {'type':1};
        
        if(qx_key != ''){
            var ownerPermission = window.localStorage.getItem('authories');
            var arr = ownerPermission.split(',');
            var a = permconfig[qx_key];
            for(var cur in a){
                if(arr.indexOf(cur) != -1){
                    param.type = a[cur];
                }
            }
        }else{
            param.type = 4;
        }
        data = setToken(data);
        data['type'] = param.type;
        return instance.get("department/dropdown",{
            params:data,
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

/*合同 */
const contract = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("contract/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("contract/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.post("contract/delete",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("contract/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //新增页面下拉框动态数据
    selectData: (data,calback) => {
        return instance.get("contract/datalist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

// 员工管理
const employee = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("user/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("user/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.post("user/del",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("user/save",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 上级重置员工密码为默认密码
    resetpassword: (data,calback) => {
        return instance.get("user/resetpassword",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 修改员工权限
    edituserpermission: (data,calback) => {
        return instance.post("user/edituserpermission",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 删除照片
    delpic: (data,calback) => {
        return instance.post("user/delimage",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}


/*回收站 */
const reclaim = {
    // 回收站    客源  列表
    resourceslist: (data,calback) => {
        return instance.get("customers/recyclebininfo",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   客源  硬删除
    resourcesdel: (data,calback) => {
        return instance.post("customers/del",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站    房源  列表
    roomlist: (data,calback) => {
        return instance.get("room/recyclebinlists",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   房源  硬删除
    roomdel: (data,calback) => {
        return instance.post("room/delhouse",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   楼盘字典  列表
    buildingdictionarylist: (data,calback) => {
        return instance.get("wordbook/recyclebinwordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   楼盘字典  硬删除
    buildingdictionarydel: (data,calback) => {
        return instance.get("wordbook/delrecyclebook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   部门  列表
    departmentlist: (data,calback) => {
        return instance.get("department/recycle",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   员工  列表
    stafflist: (data,calback) => {
        return instance.get("user/recyclebinuser",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   员工  硬删除
    deletepermanently: (data,calback) => {
        return instance.post("user/deletepermanently",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   工作总结  列表
    recyclebinlist: (data,calback) => {
        return instance.get("work/recyclebinlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 回收站   工作总结  硬删除
    recyclebindel: (data,calback) => {
        return instance.get("work/hard",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

//排行榜
const rankinglist = {
    /*员工业绩排行*/
    achievementranking: (data,calback) => {
        return instance.get("statistics/achievementlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*员工录入房源数量排行*/
    entryroomranking: (data,calback) => {
        return instance.get("statistics/roomachievementlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*员工录入客源数量排行*/
    entryresourcesranking: (data,calback) => {
        return instance.get("statistics/customerachievementlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*员工房源跟进数据排行*/
    roomfollowranking: (data,calback) => {
        return instance.get("statistics/roomfollownum",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*员工录入客源跟进数量排行*/
    resourcesfollowranking: (data,calback) => {
        return instance.get("statistics/customersfollownum",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

// 诚意金管理
const earnestmoney = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("earnest/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("earnest/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.get("earnest/delete",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("earnest/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 添加（编辑）页下拉框数据
    datalist: (data,calback) => {
        return instance.get("earnest/datalist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

//工作总结管理
const worksummary = {
     /*列表*/
     list: (data,calback) => {
        return instance.get("work/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*所需信息*/
    info: (data,calback) => {
        return instance.get("work/page",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("work/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.get("work/soft",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("work/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*经理点评*/
    comment: (data,calback) => {
        return instance.post("work/managerreview",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

//楼盘字典
const buildingdictionary = {
    /*列表*/
    list: (data,calback) => {
        return instance.get("wordbook/listwordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*添加页面下拉数据*/
    optdata: (data,calback) => {
        return instance.get("wordbook/datawordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*增*/
    add: (data,calback) => {
        return instance.post("wordbook/addwordbook",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*删*/
    del: (data,calback) => {
        return instance.get("wordbook/delwordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*改*/
    edit: (data,calback) => {
        return instance.post("wordbook/savewordbook",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 列表*/
    plist: (data,calback) => {
        return instance.get("wordbook/listbuilding",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 添加页面下拉数据*/
    poptdata: (data,calback) => {
        return instance.get("wordbook/wordbooktype",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 批量生成*/
    batchadd: (data,calback) => {
        return instance.post("wordbook/batchaddbuilding",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 增*/
    padd: (data,calback) => {
        return instance.post("wordbook/addbuilding",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 删*/
    pdel: (data,calback) => {
        return instance.get("wordbook/delbuilding",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*楼盘字典 - 栋座信息 改*/
    pedit: (data,calback) => {
        return instance.post("wordbook/savebuilding",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 楼盘字典  详细信息
    infos: (data,calback) => {
        return instance.get("wordbook/infowordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*添加房源时检索楼盘字典*/
    search: (data,calback) => {
        return instance.get("wordbook/searchwordbook",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*根据楼盘字典获取该楼盘下面的栋座*/
    pedestal: (data,calback) => {
        return instance.get("wordbook/getbuilding",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*根据栋座的名称获取该栋座下的所有位置信息*/
    positions: (data,calback) => {
        return instance.get("wordbook/getposition",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*根据楼盘字典名字获取楼盘ID*/
    getid: (data,calback) => {
        return instance.get("wordbook/getbookid",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    /*根据楼盘字典名字获取栋座位置下拉数据*/
    getbuilding: (data,calback) => {
        return instance.get("wordbook/getbuilding",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

// 辅助接口
const common = {
    // 省市区三级联动
    area: (data,calback) => {
        return instance.get("area",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //获取客户端默认区
    defaultCity: (data,calback) => {
        return instance.get("city",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 日志列表
    loglist: (data,calback) => {
        return instance.get("log/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 规则列表
    rulelist: (data,calback) => {
        return instance.get("moverule/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 规则   添加、编辑 下拉数据
    ruleoptdata: (data,calback) => {
        return instance.get("moverule/datalist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 新增规则
    addrule: (data,calback) => {
        return instance.post("moverule/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 修改规则
    editrule: (data,calback) => {
        return instance.post("moverule/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 删除规则
    delrule: (data,calback) => {
        return instance.post("moverule/delete",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 立即执行所有启用规则
    implementrule: (data,calback) => {
        return instance.post("moverule/executionrule",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 员工转房客+工作总结
    srafftransfertenant: (data,calback) => {
        return instance.post("moverule/moves",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 员工转房客转出类目数据
    staffoptdata: (data,calback) => {
        return instance.get("moverule/movedata",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 员工转房客+工作总结
    srafftransfertenant: (data,calback) => {
        return instance.post("moverule/moves",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 员工房客公私相互转换页面检索条件数据
    publicoptdata: (data,calback) => {
        return instance.get("moverule/turndata",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 员工房客公-私 私-公相互转换
    publicprivate: (data,calback) => {
        return instance.post("moverule/turns",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

//权限
const permission = {
    //房源客源其他  下拉数据
    permission: (data,calback) => {
        return instance.get("core/permissionpage",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //客源  下拉数据
    customerspermission: (data,calback) => {
        return instance.get("core/customerspermissionpage",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    //其他  下拉数据
    otherpermission: (data,calback) => {
        return instance.get("core/otherpermissionpage",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

// 权限
const duties = {
    // 列表
    list: (data,calback) => {
        return instance.get("position/list",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 添加职务
    add: (data,calback) => {
        return instance.post("position/add",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 修改职务
    edit: (data,calback) => {
        return instance.post("position/edit",setToken(data)).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 删除职务
    del: (data,calback) => {
        return instance.get("position/delete",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 职务  下拉框数据
    getpositionlist: (data,calback) => {
        return instance.get("user/getpositionlist",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
    // 批量修改职务下员工权限
    batchedit: (data,calback) => {
        return instance.get("position/editpositiondownuserpermission",{
            params:setToken(data),
            paramsSerializer: function(params) {
                return Qs.stringify(params, {arrayFormat: 'brackets'})
            }
        }).then((res)=>{
            calback('success',res)
        }).catch(error=>{
            calback('error',error.response)
        })
    },
}

export default {baseurl,resources,login,signin,room,types,department,contract,reclaim,common,employee,rankinglist,earnestmoney,worksummary,buildingdictionary,permission,duties}