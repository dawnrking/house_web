
//公共方法文件

exports.install = function (Vue, options) {
    // 字符串转数字
    Vue.prototype.transformInt = function (options){
        
        if( typeof(options) == 'string'){
            if(options == ''){
                return '';
            }else{
                return parseInt(options);
            }
            
        }else{
            return options;
        }

    };
    // 数字转字符串
    Vue.prototype.transformStr = function (options){
        if( typeof(options) == 'number'){
            if(options == ''){
                return '';
            }else{
                return options.toString();
            }
            
        }else{
            return options;
        }

    };
    // json对象转字符串
    Vue.prototype.JtransformStr = function (options){
        
        if( typeof(options) == 'object'){
            
            return JSON.stringify(options); 
            
        }else{
            return options;
        }

    };
};