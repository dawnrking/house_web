var authories = window.localStorage.getItem("authories");

if(authories){
    var authories_arr = authories.split(',');
}
// console.log(authories_arr)
var system_sid = window.localStorage.getItem('system_sid');
var benbuid = window.localStorage.getItem('did');
var benrenid = window.localStorage.getItem('uid');
var endtime = window.localStorage.getItem('end_time');
export default
{
    //客源权限
    "qx_keyuan_chakan" : {"108":4, "109":3, "110":2, "111":1}, // 所有部门108=>4，跨部109=>3，本部110=>2，本人111=>1
    "qx_keyuan_tianjia" : {"112":4, "113":3, "114":2, "115":1},
    "qx_keyuan_benren_chakan_keyuan":'111', 
    "qx_keyuan_benren_tianjia" : "115",
    "qx_keyuan_liebiao_shanchu_function":(curData, callback)=>{
        var res = false;
        if(authories_arr.indexOf('120') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('121')!= -1 && curData.c_store_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('122')!= -1 && curData.c_store_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('123')!= -1 && curData.c_employees_id == benrenid ){ //本人
            res = true;
        }
        if(!res){
            alert('您没有权限删除此数据');
        }
        callback(res);
    },
    "qx_keyuan_liebiao_bianji_function":(curData)=>{
        // alert(JSON.stringify(curData));
        var res = false;
        if(authories_arr.indexOf('116') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('117')!= -1 && curData.c_store_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('118')!= -1 && curData.c_store_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('119')!= -1 && curData.c_employees_id == benrenid ){ //本人
            res = true;
        }
        return res;
    },
    "qx_keyuan_kehuxingming":"85",//客户姓名
    "qx_keyuan_kehudianhua":"86",//客户电话
    "qx_keyuan_lianxirenxingming":"87",//联系人姓名
    "qx_keyuan_lianxirendianhua":"88",//联系人电话
    "qx_keyuan_kehuzhuzhi":"89",//客户住址
    "qx_keyuan_laiyuan":"90",//来源
    "qx_keyuan_dengji":"91",//等级
    "qx_keyuan_jiaoyileixing":"92",//交易类型
    "qx_keyuan_zhuangtai":"93",//状态
    "qx_keyuan_sike":"94",//私客
    "qx_keyuan_youzhike":"95",//优质客
    "qx_keyuan_jutidizhi":"96",//具体地址
    "qx_keyuan_fangxing":"97",//房型
    "qx_keyuan_zuidamianji":"98",//最大面积
    "qx_keyuan_zuixiaomianji":"99",//最小面积
    "qx_keyuan_zuidashoujia":"100",//最大售价
    "qx_keyuan_zuixiaoshoujia":"101",//最小售价
    "qx_keyuan_weituoshijian":"102",//委托时间
    "qx_keyuan_weituofangshi":"103",//委托方式
    "qx_keyuan_yongtu":"104",//用途
    "qx_keyuan_fukuanleixing":"105",//付款类型
    "qx_keyuan_beizhu":"106",//备注

    "qx_keyuan_chakansike":"107",//查看私客
    "qx_keyuan_kankehububixiegenjin":"163",//看客户不必写跟进

    //房源权限
    "qx_room_chakan" : {"1":4, "2":3, "3":2, "4":1},
    "qx_room_tianjia" : {"5":4, "6":3, "7":2, "8":1},
    "qx_room_benren_chakan":'4', 
    "qx_room_benren_tianjia" : "8",
    "qx_room_liebiao_shanchu_function":(curData, callback)=>{
        var res = false;
        if(authories_arr.indexOf('13') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('14')!= -1 && curData.r_d_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('15')!= -1 && curData.r_d_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('16')!= -1 && curData.source_uid == benrenid ){ //本人
            res = true;
        }
        if(!res){
            alert('您没有权限删除此数据');
        }
        callback(res);
    },
    "qx_room_liebiao_bianji_function":(curData)=>{
        var res = false;
        if(authories_arr.indexOf('9') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('10')!= -1 && curData.r_d_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('11')!= -1 && curData.r_d_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('12')!= -1 && curData.source_uid == benrenid ){ //本人
            res = true;
        }
        return res;
    },

    //上传图片
    "qx_room_shangchauntupian":(curData, callback)=>{
        console.log(curData);
        var res = false;
        if(authories_arr.indexOf('55') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('56')!= -1 && curData.r_d_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('57')!= -1 && curData.r_d_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('58')!= -1 && curData.source_uid == benrenid ){ //本人
            res = true;
        }
        callback(res);
    },

    //修改图片
    "qx_room_xiugaitupian":(curData, callback)=>{
        console.log(curData);
        var res = false;
        if(authories_arr.indexOf('59') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('60')!= -1 && curData.r_d_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('61')!= -1 && curData.r_d_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('62')!= -1 && curData.source_uid == benrenid ){ //本人
            res = true;
        }
        callback(res);
    },

    //查看跟进
    "qx_room_chakangenjin":{"63":4,"64":3,"65":2,"66":1},
    "qx_genjin_benren_chakan":"66",

    //点评跟进
    // 房源点评
    "qx_room_dianpinggenjin":(curData)=>{
        var res = false;
        if(authories_arr.indexOf('67') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('68')!= -1 && curData.f_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('69')!= -1 && curData.f_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('70')!= -1 && curData.f_user_id == benrenid ){ //本人
            res = true;
        }
        return res;
    },
    //客源点评
    "qx_keyuan_dianpinggenjin":(curData)=>{
        var res = false;
        if(authories_arr.indexOf('67') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('68')!= -1 && curData.cf_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('69')!= -1 && curData.cf_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('70')!= -1 && curData.cf_user_id == benrenid ){ //本人
            res = true;
        }
        return res;
    },

    //添加跟进
    "qx_room_tianjiagenjin":{"71":4,"72":3,"73":2,"74":1},
    "qx_genjin_benren_tianjia":"74",



    //修改跟进 客源
    "qx_ke_xiugaigenjin":(curData)=>{
       
        var res = false;
        if(authories_arr.indexOf('75') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('76')!= -1 && curData.cf_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('77')!= -1 && curData.cf_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('78')!= -1 && curData.cf_user_id == benrenid ){ //本人
            res = true;
        }else if(authories_arr.indexOf('79')!= -1 && curData.cf_user_id == benrenid && parseInt(curData.cf_created_at) < parseInt(endtime)){ //本人当天
            res = true;
        }
       return res;
    },


    //删除跟进 客源
    "qx_ke_shanchugenjin":(curData, callback)=>{
        var res = false;
        if(authories_arr.indexOf('80') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('81')!= -1 && curData.cf_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('82')!= -1 && curData.cf_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('83')!= -1 && curData.cf_user_id == benrenid ){ //本人
            res = true;
        }else if(authories_arr.indexOf('83')!= -1 && curData.cf_user_id == benrenid && parseInt(curData.cf_created_at) < parseInt(endtime)){ //本人当天
            res = true;
        }
        callback(res);
    },

     //修改跟进 房源
     "qx_room_xiugaigenjin":(curData)=>{
        var res = false;
        if(authories_arr.indexOf('75') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('76')!= -1 && curData.f_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('77')!= -1 && curData.f_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('78')!= -1 && curData.f_user_id == benrenid ){ //本人
            res = true;
        }else if(authories_arr.indexOf('79')!= -1 && curData.f_user_id == benrenid && parseInt(curData.f_created_at) < parseInt(endtime)){ //本人当天
            res = true;
        }
        return res;
    },


    //删除跟进 房源
    "qx_room_shanchugenjin":(curData,callback)=>{
        var res = false;
        if(authories_arr.indexOf('80') != -1){ // 所有
            res = true;
        }else if(authories_arr.indexOf('81')!= -1 && curData.f_department_id != system_sid){ //跨步
            res = true;
        }else if(authories_arr.indexOf('82')!= -1 && curData.f_department_id == benbuid){ // 本部
             res = true;
        }else if(authories_arr.indexOf('83')!= -1 && curData.f_user_id == benrenid ){ //本人
            res = true;
        }else if(authories_arr.indexOf('83')!= -1 && curData.f_user_id == benrenid && parseInt(curData.f_created_at) < parseInt(endtime)){ //本人当天
            res = true;
        }
        console.log(123);
        if(!res){
            alert('您没有权限删除此数据');
        }
        callback(res);
    },

    

    //字段权限

    //楼盘字典
    "qx_room_loupanzidian":"17",
    //栋座
    "qx_room_dongzuo":"18",
    //位置
    "qx_room_weizhi":"19",
    //房号
    "qx_room_fanghao":"20",
    //楼层
    "qx_room_louceng":"21",
    //总层
    "qx_room_zongceng":"22",
    //用途
    "qx_room_yongtu":"23",
    //房型
    "qx_room_fangxing":"24",
    //面积
    "qx_room_mianji":"25",
    //交易
    "qx_room_jiaoyi":"26",
    //状态
    "qx_room_zhuangtai":"27",
    //价格
    "qx_room_jiage":"28",
    //低价
    "qx_room_dijiage":"29",
    //委托方式
    "qx_room_weituofangshi":"30",
    //委托时间
    "qx_room_weituoshijian":"31",
    //委托编号
    "qx_room_weituobianhao":"32",
    //来源
    "qx_room_laiyuan":"33",
    //备注
    "qx_room_beizhu":"34",
    //房源明细
    "qx_room_fangyuanmingxi":"35",
    //出售类型
    "qx_room_chushouleixing":"36",
    //盘类型
    "qx_room_panleixing":"37",
    //业主姓名
    "qx_room_yezhuxingming":"38",
    //业主电话
    "qx_room_yezhudianhua":"39",
    //产权类型
    "qx_room_chanquanleixing":"40",
    //证件类型
    "qx_room_zhengjianleixing":"41",
    //付款类型
    "qx_room_fukuanleixing":"42",
    //付佣类型
    "qx_room_fuyongleixing":"43",
    //看房类型
    "qx_room_kanfangleixing":"44",
    //有效-预定
    "qx_room_youxiaoyuding":"45",
    //有效-已售/我售
    "qx_room_youxiaoyishou":"46",
    //有效-已租/我租
    "qx_room_youxiaoyizu":"47",
    //有效-其他
    "qx_room_youxiaoqita":"48",
    //预定-非预定
    "qx_room_yudingfeiyuding":"49",
    //非有效-有效
    "qx_room_feiyouxoapyuding":"50",
    //查看私盘
    "qx_room_chakansipan":"51",
    //查看特盘
    "qx_room_chakantepan":"52",
    //查看下定盘
    "qx_room_chakanxiadingpan":"53",
    //查看斡旋盘
    "qx_room_chakanwoxuanpan":"54",
    //164	查看封盘  还没有添加该权限
    "qx_room_chakanfengpan":"164",
    //看业主不必写跟进
    "qx_room_kanyezhububixiegenjin":"162",
    



    //其他

    //打卡列表
    "qx_qita_dakaliebiao_chakan":{"124":4,"125":3,"126":2,"127":1},
    "qx_qita_dakaliebiao_benren_chakan":"127",
    
    //员工
    "qx_qita_yuangong":{"158":4,"159":3,"160":2,"161":1},
    "qx_qita_yuangong_benren":"161",

    //经理点评
    "qx_qita_jinglidianping":"128",

    //删除工作总结
    "qx_qita_shanchugongzuozongjie":"129",

    //编辑工作总结
    "qx_qita_bianjigongzuozongjie":"152",

    //工作总结详情
    "qx_qita_xiangqinggongzuozongjie":"153",

    //添加合同
    "qx_qita_tianjiahetong":"130",

    //修改合同
    "qx_qita_xiugaihetong":"131",

    //删除合同
    "qx_qita_shanchuhetong":"132",

    //添加诚意金
    "qx_qita_tianjiachengyijin":"133",

    //修改诚意金
    "qx_qita_xiugaichengyijin":"134",

    //删除诚意金
    "qx_qita_shanchuchengyijin":"135",

    //删除回收站信息
    "qx_qita_shanchuhuishouzhanxinxi":"136",

    //还原回收站信息
    "qx_qita_huanyuanhuishouzhan":"137",

     //查看楼盘字典
    "qx_qita_chankanloupanzidian":"138",

    //添加楼盘字典
    "qx_qita_tianjialoupanzidian":"139",

    //修改楼盘字典
    "qx_qita_xiugailoupanzidian":"140",

    //删除楼盘字典
    "qx_qita_shanchuloupanzidian":"141",

    //查看部门
    "qx_qita_chakanbumen":"142",

    //添加部门
    "qx_qita_tianjiabumen":"143",

    //修改部门
    "qx_qita_xiugaibumen":"144",

    //删除部门
    "qx_qita_shanchubumen":"145",

    //添加员工
    "qx_qita_tianjiayuangong":"147",

    //修改员工
    "qx_qita_xiugaiyuangong":"148",

    //删除员工
    "qx_qita_shanchuyuangong":"149",

    //重置员工密码
    "qx_qita_chongzhimima":"150"
}
