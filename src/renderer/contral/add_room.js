import '../style/add_room.css'
import conf from '../config/userconf.js'
import permconfig from "../config/permconfig.js"
import $ from "jquery";
export default {
    data() {
        var validatePhone = (rule,value,callback) => {    //正则手机号

            var MobileRegex = /^1[3|4|5|7|8][0-9]{9}$/;
            if (!MobileRegex.test(value)) {
                callback(new Error('手机号格式不正确！'))
            } else {
                callback();
            }
        };
        return {
            customer:[],//客户信息
            r_id: '',  // 房源id
            c_id: '',  // 客源id
            important_information: '',
            followlistdata: [],  //跟进列表数据
            followcurrent: 1,  //跟进页码
            resourcesdialogVisible: false,  //匹配的客源信息弹框  显示隐藏
            editfollowdialogVisible: false,  //跟进弹框  隐藏显示
            followsearch: {},  //跟进查询条件
        }
    },
    methods: {
        // //跟进列表
        followlist(res,data=Object()){
            // console.log(res,data);
            data.f_room_id = this.r_id;
            data.page = this.followcurrent;
            conf.room.getfollowlist(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res);
                    this.followlistdata = res.data.data;
                }else if (state == 'success'&&res.data.code != 0){
                    
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        // //跟进切换页码
        followCurrentChange(val){
            this.followcurrent = val;
            this.followlist();
        },
        //查看匹配的客源信息
        lookresources(row){
            // console.log(row); 
            //将数据存储sessionStorage，减少http请求，避免刷新頁面時數據丟失   （弹框内容为修改状态页面）
            sessionStorage.setItem("editresourcesrow", JSON.stringify(row));
            
            //子组件重新渲染
            this.resourcesdialogVisible = false;
            this.$nextTick( function () {
                this.resourcesdialogVisible = true; 
            });

        },

        //接收子组件数据   添加修改成功   
        roomkeepievent(res,tjstatus){
            console.log('----------------房源添加修改成功---------------',res,tjstatus);
            // this.pagestatus = tjstatus;
            this.r_id = res.data.data.rid;  //房源id
            this.customer = res.data.data.customers_list;  //客源信息

            //客源  底部信息 内容
            this.important_information = res.data.data.important_information;
            this.followlist();  //获取房源跟进列表
            
        },
        //接收子组件数据   底部跟进数据  
        roomfollowievent(res){
            console.log('----------------跟进---------------',res);
            if(res){
                this.r_id = res; 
            }
            this.followlist();
        },
        //接收子组件数据   客源信息
        roomievent(res){
            this.customer = res;
        },
        //接收子组件数据   客源信息
        roommaininfoievent(res){
            this.important_information = res;
        },
        //编辑跟进
        showfollowlist(row, event){
            console.log(row);
            this.followsearch.type = 3;   //房客源类型 1房源提醒 2客源提醒   3为房客源普通修改
            this.followsearch.id = '';  //房id或客id
            this.followsearch.cfid = row.f_id;  //房跟进Id 或客跟进id

            this.editfollowdialogVisible = true;

        },
        //接收子组件
        followievent(){
            this.followlist();
            this.editfollowdialogVisible = false;
            
        },
        //格式化   跟进内容
        followFun(row, column, cellValue, index){
            // console.log(row, column, cellValue, index);
            switch (row.f_follow_up_way) {
                case 1:
                    return cellValue + '（去电）'
                case 2:
                    return cellValue + '（来电）'
                case 3:
                    return cellValue + '（看房）'
                case 4:
                    return cellValue + '（勘察）'
                case 5:
                    return cellValue + '（来访）'
                case 6:
                    return cellValue + '（拜访）'
                case 7:
                    return cellValue + '（派单）'
                case 8:
                    return cellValue + '（短信）'
                case 9:
                    return cellValue + '（邮件）'
                case 10:
                    return cellValue + '（信函）'
                case 11:
                    return cellValue + '（申请）'
                case 12:
                    return cellValue + '（修改）'
                case 13:
                    return cellValue + '（保留）'
                case 14:
                    return cellValue + '（其他）'
            }
        },
    },
    mounted (){
        this.$nextTick(function(){
            
        })

        var dialog = null;
        var startX,startY,dialogX, dialogY;
        window.onmousemove = function(e){
            if(dialog){
                var x = e.clientX;
                var y = e.clientY;
                var pyx = x-startX;
                var pyy = y-startY;
                dialog.css({'margin-top':(dialogY+pyy)+"px", 'margin-left':(dialogX+pyx)+"px" });
            }
        };

    
        $(document).on("mousedown", ".el-dialog__header",function(e){
            startX = e.clientX;
            startY = e.clientY;
            var dialogt = $(this).closest(".el-dialog");
            dialogY = Number(dialogt.css("margin-top").replace("px",""));
            dialogX = Number(dialogt.css("margin-left").replace("px",""));
            dialog = dialogt;
        })
        $(document).on("mouseup", ".el-dialog__header",function(){
            dialog = null;
        })
    },
}