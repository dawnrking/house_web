import '../style/add_room.css'
import conf from '../config/userconf.js'
export default {
    data() {
        var validatePhone = (rule,value,callback) => {    //正则手机号

            var MobileRegex = /^1[3|4|5|7|8][0-9]{9}$/;
            if (!MobileRegex.test(value)) {
                callback(new Error('手机号格式不正确！'))
            } else {
                callback();
            }
        };
        var isCardNo = (rule,value,callback) => {    //正则身份证
                                
            var MobileRegex = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            // var MobileRegex = /(^\d{14}[[0-9],0-9xX]$)|(^\d{17}[[0-9],0-9xX]$)/;
            if (!MobileRegex.test(value)) {
                callback(new Error('身份证格式不正确！'))
            } else {
                callback();
            }
        };
        return {
            form: {
                c_user_name: '',
                c_user_phone: '',
                c_contact: '',
                c_contact_type: '',
                c_id_card: '',
                c_address: '',
                c_user_level: '',
                c_live_type: '',
                c_professional_type: '',
                c_user_intention: '',
                c_time_limit: '',
                c_trading_type: '',
                c_buy_status: '',
                c_private_guest: true,  //私客
                c_quality: false,  //优质客
                c_secondary_area: '',
                c_area_address: '',
                c_room: '',  //房型 （几室）
                c_room_json: {
                    office_s: '',
                    guard_s: '',
                    balcony_s: '',
                    staff_s: '',
                    dormitory_s: '',
                    work_office: '',
                    vacant_land: '',
                },
                c_position: '',
                c_level: '',
                c_planning: '',
                c_acreage_min: '',
                c_acreage_max: '',
                c_entrust_time: '',
                c_price_min: '',
                c_price_max: '',
                c_rent_price_min: '',
                c_rent_price_max: '',
                c_source_type: '',
                c_entrust_type: '',
                c_use_type: 3,  //用途    默认 3住宅
                c_floor: '',
                c_industry: '',
                c_electricity: '',
                c_parking: '',
                c_room_type: '',
                c_toward: '',
                c_number_of_layers: '',
                c_decorate_type: '',
                c_wc: '',
                c_situation: '',
                c_fence: '',
                c_payment_type: '',
                c_complete: [],
                c_lot: [],
                c_pay_type: '',
                c_remark: '',
                c_store_id: '',
                c_employees_id: '',
            },
            rules:{
                c_user_name: [
                    { required: true, message: '请输入客户姓名', trigger: 'blur' }
                ],
                c_user_phone: [
                    { required: true, message: '请输入客户手机号', trigger: 'blur' },
                    { validator: validatePhone, trigger: 'blur'},
                ],
                c_trading_type: [
                    { required: true, message: '请选择交易类型', trigger: 'change' }
                ],
                c_buy_status: [
                    { required: true, message: '请选择购买状态', trigger: 'change' }
                ],
                c_private_guest: [
                    { required: true, message: '请输入房号', trigger: 'blur' }
                ],
                c_secondary_area: [
                    { required: true, message: '请选择区县', trigger: 'change' }
                ],
                c_acreage_max: [
                    { required: true, message: '请输入面积最大值', trigger: 'blur' }
                ],
                c_acreage_min: [
                    { required: true, message: '请输入面积最小值', trigger: 'blur' }
                ],
                c_entrust_time: [
                    { required: true, message: '请选择委托时间', trigger: 'change' }
                ],
                c_source_type: [
                    { required: true, message: '请选择来源类型', trigger: 'change' }
                ],
                c_use_type: [
                    { required: true, message: '请选择用途类型', trigger: 'change' }
                ],
                c_store_id: [
                    { required: true, message: '请选择部门', trigger: 'change' }
                ],
                c_employees_id: [
                    { required: true, message: '请选择员工', trigger: 'change' }
                ],
                c_id_card: [
                    { required: false, message: '请填写身份证号', trigger: 'blur' },
                    { validator: isCardNo, trigger: 'blur'},
                ],
            },
            housing:[],//房源信息
            transaction: 1,// 交易类型  1 求购、2 求租 、3 租购
            options: '',  //所有下拉数据
            customeroptlist: '',  //客源下拉数据
            c_id: '',  // 客源id
            followlistdata: [],  //跟进列表数据
            followcurrent: 1,  //跟进页码
            maininfo: '',
            trading_type_opt: [  //交易类型下拉数据
                {name: '求购',id: '1'},
                {name: '求租',id: '2'},
            ],
            pagestatus: 1,   //页面状态   新增    修改
            resourcesdialogVisible: false,  //匹配的房源信息弹框  显示隐藏
            editfollowdialogVisible: false,  //跟进弹框  隐藏显示
            followsearch: {},  //跟进查询条件
        }
    },
    methods: {
        //接收子组件数据   添加修改成功   
        ievent(res,tjstatus){
            console.log('------------------客源添加修改成功--------------------',res,tjstatus);
            this.pagestatus = tjstatus;
            this.c_id = res.data.data.c_id;  //客源id
            this.housing = res.data.data.room_list;  //房源信息

            //客源  底部信息 内容
            this.maininfo = res.data.data.customersFastInfo;
            
        },
        //接收子组件数据   底部跟进数据  
        followievent(res){
            // console.log(res);
            if(res){
                this.c_id = res; 
            }
            this.followlist();
        },
        //接收子组件数据   房源信息
        housingievent(res){
            // console.log(res);
            if(res){
                this.housing = res;
            }
        },
        //接收子组件数据   房源信息
        maininfoievent(res){
            // console.log(res);
            if(res){
                this.maininfo = res;
            }
        },
        //查看匹配的客源信息
        lookresources(row){
            console.log(row);
            //将数据存储sessionStorage，减少http请求，避免刷新頁面時數據丟失   （弹框内容为修改状态页面）
            sessionStorage.setItem("editroomrow", JSON.stringify(row));
            
            //子组件重新渲染
            this.resourcesdialogVisible = false;
            this.$nextTick( function () {
                this.resourcesdialogVisible = true; 
            });
        },
        //获取房源下拉
        optlist(data=Object()){
            if(window.localStorage.roomoptdata){
                this.customeroptlist = JSON.parse(window.localStorage.getItem("roomoptdata"));
                if(this.tjstatus == 1){
                    this.form.c_entrust_time = JSON.parse(window.localStorage.getItem("roomoptdata")).day;
                }
            }else{
                conf.room.optlist(data,(state,res)=>{
                    if (state == 'success'&&res.data.code == 0){
                        // console.log(res);
                        this.options = res.data.data;

                        let obj = JSON.stringify(res.data.data); //转化为JSON字符串
                        window.localStorage.setItem("roomoptdata", obj);

                    }else if (state == 'success'&&res.data.code != 0){
                        if(res.data.code == 401){
                            this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                        }else{
                            this.$message({
                                type: 'info',
                                message: res.data.msg
                            });
                        }
                    }
                })
            }
            
        },
        /*返回列表 */
        cancel (formName) {
            this.$router.push({name:'resources'})
        },
        /*提交 */
        submit (formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.judgeprimise();
                } else {
                    console.log('error submit!!');
                    return false;
                }
            });
        },
        //跟进列表
        followlist(data=Object()){
            
            data.page = this.followcurrent;
            data.cf_c_id = this.c_id;
            // console.log(data);
            conf.resources.follow(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res);
                    this.followlistdata = res.data.data;
                    // console.log(this.followlistdata);
                }else if (state == 'success'&&res.data.code != 0){
                    
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        //跟进切换页码
        followCurrentChange(val){
            this.followcurrent = val;
            this.followlist();
        },
        //编辑跟进
        showfollowlist(row, event){
            console.log(row);
            this.followsearch.type = 4;   //房客源类型 1房源提醒 2客源提醒   3,4为房客源普通修改
            this.followsearch.id = '';  //房id或客id
            this.followsearch.cfid = row.cf_id;  //房跟进Id 或客跟进id

            this.editfollowdialogVisible = true;

        },
        //接收子组件
        getfollowievent(){
            this.followlist();
            this.editfollowdialogVisible = false;
        },
        //格式化   跟进内容
        followFun(row, column, cellValue, index){
            // console.log(row, column, cellValue, index);
            switch (row.cf_fllow_type) {
                case 1:
                    return cellValue + '（去电）'
                case 2:
                    return cellValue + '（来电）'
                case 3:
                    return cellValue + '（看房）'
                case 4:
                    return cellValue + '（勘察）'
                case 5:
                    return cellValue + '（来访）'
                case 6:
                    return cellValue + '（拜访）'
                case 7:
                    return cellValue + '（派单）'
                case 8:
                    return cellValue + '（短信）'
                case 9:
                    return cellValue + '（邮件）'
                case 10:
                    return cellValue + '（信函）'
                case 11:
                    return cellValue + '（申请）'
                case 12:
                    return cellValue + '（修改）'
                case 13:
                    return cellValue + '（保留）'
                case 14:
                    return cellValue + '（其他）'
            }
        },
    },
    created (){
        this.$nextTick(function(){
            this.optlist();  //房源下拉数据
        })
    },
}