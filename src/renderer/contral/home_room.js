import conf from '../config/userconf.js'
import permconfig from "../config/permconfig.js"
export default {
    data() {
        var validateTel = (rule,value,callback) => {    //正则手机号

            var MobileRegex = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;
            if (!MobileRegex.test(value)) {
                callback(new Error('手机号格式不正确！'))
            } else {
                callback();
            }
        };
        return {
            list:[],
            searchData:{  //查询
                // a_id:'',
                building_id:'',
                type_of_house_sale:'',
                use_type:'',
                r_acreage_begin:'',
                r_acreage_end:'',
                status:'',
                floor_begin:'',
                floor_end:'',
                r_d_id:'',
                source_uid:'',
                trading_type:'',
                small_price:'',
                big_price:'',
                date_type: '1',
                start_time:'',
                end_time:'',
                checking_way: false,
                housing_types:'',
                roomtype:'',
                numlayers:'',
                oriented:'',
                keywords:'',
            },
            roomcurrent: 1,  //当前页码
            options: [],   //查询下拉数据
            trading_type_opt: [  //交易类型下拉数据
                {name: '出售',id: '1'},
                {name: '出租',id: '2'},
            ],
            date_type_opt: [  //日期类型下拉数据
                {name: '录入时间',id: '1'},
                {name: '最后跟进时间',id: '2'},
                {name: '交房时间',id: '3'},
                {name: '建房年代 ',id: '4'},
                {name: '委托时间',id: '5'},
            ],
            optionName: [],  //部门下拉数据
            userTypes: [],  // 员工下拉数据
            typeOne: 1,  //用途更改    所填资料类型
            buildingOpt: [],  //楼盘字典的下拉
            buildingId: '',  // 楼盘字典id
            maininfo: '',  //房源主要信息
            followlist: [],  //跟进列表数据
            userinfo: '',   //上班考勤  内容
            worktime: '',   //上班考勤  内容
            info: '',   //上班考勤  内容
            dialogVisible: false,  //上班考勤 弹框  显示隐藏
            reminddialogVisible: false,  //业务提醒  弹框   显示隐藏
            reminddata: [],  //业务提醒   数据
            roomdialogVisible: false,  //房源信息   弹框   显示隐藏
            resourcesdialogVisible: false,  //客源信息   弹框   显示隐藏
            followdialogVisible: false,  //跟进提醒弹框
            followsearch: {},  //跟进查询条件
            followtitle: '',  //跟进提醒  弹框标题
            editfollowdialogVisible: false,  //编辑跟进  弹框
            aaa:'hsajfsjgfds'
      }
    },
    methods: {
        //获取房源  查询条件 下拉数据
        optlist(data=null){
            //如若接收到打卡数据，则显示打卡信息
            
            if(this.$route.query.userinfo){
                this.userinfo = this.$route.query.userinfo;
                this.worktime = this.$route.query.worktime;
                this.info = this.$route.query.info;
                this.dialogVisible = true;   //弹出打卡成功弹框
                //有房客源提醒时    则在打卡弹框关闭后显示
            }else{
                // //如若接收到业务提醒数据，则显示业务提醒信息
                if(this.$route.query.remindstatus == 1){
                    //查看是否有房客源提醒
                    conf.signin.remind(data,(state,res)=>{
                        if (state == 'success'&&res.data.code == 0){
                            // console.log(res);
                            this.reminddata = res.data.data;
                        }else if (state == 'success'&&res.data.code != 0){
                            if(res.data.code == 401){
                                this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                            }else if(res.data.code == -413){  //暂时没有业务提醒
                                
                            }else{
                                this.$message({
                                    type: 'info',
                                    message: res.data.msg
                                });
                            }
                            
                        }
                    })
                    this.reminddialogVisible = true;
                }
            }
            
            // 房源  查询条件 下拉数据
            conf.room.searchoptlist(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res);
                    this.options = res.data.data;
                    this.searchData.start_time = res.data.data.lastday;
                    this.searchData.end_time = res.data.data.day;
                    this.searchData.r_d_id = parseInt(window.localStorage.getItem('did'));
                    this.obtainUsers(this.searchData.r_d_id); 
                    this.searchData.source_uid = parseInt(window.localStorage.getItem('uid'));
                    console.log(typeof(window.localStorage.getItem('uid')),typeof(this.searchData.source_uid));
                }else if (state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        //接收子组件
        followievent(){
            this.getList();
            if(this.editfollowdialogVisible){
                this.editfollowdialogVisible = false;
            }
            if(this.followdialogVisible){
                this.followdialogVisible = false;
            }
            
        },
        //接收子组件    处理业务提醒
        processingevent(data=null){

            this.followdialogVisible = false;
            conf.signin.remind(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res);
                    this.reminddata = res.data.data;
                }else if (state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else if(res.data.code == -413){  //暂时没有业务提醒
                        this.reminddata = [];
                        this.reminddialogVisible = false;
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                    
                }
            })
        },
        //业务提醒  双击查看房客源信息
        reminddb(row, event, column){
            console.log(row);
            if(row.type == 1){   //房源
                //将数据存储sessionStorage，减少http请求，避免刷新頁面時數據丟失   （弹框内容为修改状态页面）
                sessionStorage.setItem("editroomrow", JSON.stringify(row.r_info));
                this.roomdialogVisible = true;
            }else if(row.type == 2){   //客源
                //将数据存储sessionStorage，减少http请求，避免刷新頁面時數據丟失   （弹框内容为修改状态页面）
                sessionStorage.setItem("editresourcesrow", JSON.stringify(row.c_info));
                this.resourcesdialogVisible = true;
            }
        },
        //处理跟进提醒
        follow(row){
            console.log(row);
            this.followsearch.type = row.type;   //房客源类型 1房源提醒 2客源提醒
            this.followsearch.id = row.id;  //房id或客id
            this.followsearch.cfid = row.cf_id;  //房跟进Id 或客跟进id
            this.followsearch.cfs_id = row.cfs_id;  //房客源跟进提醒Id

            if(this.followsearch.type == 1){
                this.followtitle = '房源跟进';
            }else if(this.followsearch.type == 2){
                this.followtitle = '客源跟进';
            }
            this.followdialogVisible = true;
        },
        //关闭打卡弹框
        signclose(data=null){
            this.dialogVisible = false;
            // //如若接收到业务提醒数据，则显示业务提醒信息
            if(this.$route.query.remindstatus == 1){
                //查看是否有房客源提醒
                conf.signin.remind(data,(state,res)=>{
                    if (state == 'success'&&res.data.code == 0){
                        // console.log(res);
                        this.reminddata = res.data.data;
                    }else if (state == 'success'&&res.data.code != 0){
                        if(res.data.code == 401){
                            this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                        }else if(res.data.code == -413){  //暂时没有业务提醒
                            
                        }else{
                            this.$message({
                                type: 'info',
                                message: res.data.msg
                            });
                        }
                        
                    }
                })
                this.reminddialogVisible = true;
            }
        },
        /* 列表*/
        getList (data=Object()){
            data.page = this.roomcurrent;
            conf.room.list(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    this.list = res.data.data;
                    this.maininfo = res.data.data.data[0].roomInfo;

                    var ownerPermission = window.localStorage.getItem('authories');
                    var arr = ownerPermission.split(',');
                    //查看私盘 公盘35 私盘36 封盘37 下定盘38 斡旋盘39
                    if(res.data.data.data[0].disc_type != 35){  //私盘
                        if(arr.indexOf(permconfig.qx_room_chakansipan) == -1 && res.data.data.data[0].source_uid != window.localStorage.getItem('uid')){
                            this.maininfo.secound = "【**】 ***********";
                        }
                    }else if(arr.indexOf(permconfig.qx_room_kanyezhububixiegenjin) == -1 && res.data.data.data[0].source_uid != window.localStorage.getItem('uid')){
                        var cache = window.localStorage.getItem("ROOM_PUBLIC_GUEST" + res.data.data.id);
                        if(!cache){
                            this.maininfo.secound = "【**】 ***********";
                        }
                    }

                     this.followlist = res.data.data.data[0].roomFollow;
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else if(res.data.code == -404){
                        this.list = []
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        //查询
        search(formName){
            let data = this.searchData;
            this.getList(data);
        },
        //单击某行获取改行的跟进列表、主要信息
        getfollowlist(row, event, column){
           this.maininfo = row.roomInfo;
            //  权限
            var ownerPermission = window.localStorage.getItem('authories');
            var arr = ownerPermission.split(',');
            //查看私客、看客户不必写跟进  权限

            if(row.disc_type != 35){  //私客
                if(arr.indexOf(permconfig.qx_room_chakansipan)  == -1 && row.source_uid != window.localStorage.getItem('uid')){
                    row.roomInfo.secound = "【**】 ***********";
                }
            }else if(arr.indexOf(permconfig.qx_room_kanyezhububixiegenjin) == -1 && row.source_uid != window.localStorage.getItem('uid')){
                var cache = window.localStorage.getItem("ROOM_PUBLIC_GUEST" + row.id);
                if(!cache){
                    this.maininfo.secound = "【**】 ***********";
                }
            }


            //this.maininfo = row.roomInfo;
            this.followlist = row.roomFollow;
        },
        // 切换页码
        handleCurrentChange(val) {
            this.roomcurrent = val;
            this.getList();
        },
        //修改
        edit(row){
            // console.log(row);
            //修改房源信息时 将数据存储sessionStorage，减少http请求
            sessionStorage.setItem("editroomrow", JSON.stringify(row));
            // console.log(sessionStorage.getItem("editroomrow"));
            
            this.$router.push({name : 'addroom'});
        },
        //删除
        delect(row,data=Object()){
            this.$confirm('确认删除房源 ["'+row.property_number+'"] ?', '删除房源', {  
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                permconfig.qx_room_liebiao_shanchu_function(row, (res)=>{
                    if(!res){
                        return;
                    }
                    data.id = row.id;
                    conf.room.reclaim(data,(state,res)=>{
                        if (state == 'success'&&res.data.code == 0){
                            this.getList();
                            this.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                        }else if(state == 'success'&&res.data.code != 0){
                            if(res.data.code == 401){
                                this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                            }else{
                                this.$message({
                                    type: 'info',
                                    message: res.data.msg
                                });
                            }
                        }
                    })
                })

            }).catch(() => {
                this.$message({
                type: 'info',
                message: '已取消删除'
                });          
            });
        },
        //楼盘字典检索
        buildingquerySearchAsync(queryString,cb,data=Object()){
            
            if(queryString != ''){
                data.p_id = this.options.province_code;
                data.c_id = this.options.city_code;
                // data.a_id = this.searchData.a_id;
                data.w_spell = queryString;
                conf.buildingdictionary.search(data,(state,res)=>{
                    if (state == 'success'&&res.data.code == 0){
                        // console.log(res);
                        this.buildingOpt = res.data.data;
                        cb(this.buildingOpt);
                        
                    }else if (state == 'success'&&res.data.code != 0){
                        if(res.data.code == 401){
                            this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                        }else if (res.data.code == -404){   //暂无数据
                            this.buildingOpt = [{
                                value: '无数据'
                            }];
                            cb(this.buildingOpt);  
                        }else{
                            this.$message({
                                type: 'info',
                                message: res.data.msg
                            });
                        }
                    }
                })
            }
            
        },
        // 用途 change
        typeChange(val){
            console.log(val);
            //3 住宅 、4 商住 、14 其他
            //5 商铺 、6 网点 、10 铺厂 
            //7 写字楼 、9 写厂
            //8 厂房
            //11 仓库
            //12 地皮 
            //13 车位
            switch(val){
                case 3:
                    this.typeOne = 1;
                    this.initOne();
                    break;
                case 4:
                    this.typeOne = 1;
                    this.initOne();
                    break;
                case 5:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 6:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 7:
                    this.typeOne = 3;
                    this.initThree();
                    break;
                case 8:
                    this.typeOne = 4;
                    this.initFour();
                    break;
                case 9:
                    this.typeOne = 3;
                    this.initThree();
                    break;
                case 10:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 11:
                    this.typeOne = 5;
                    this.initFive();
                    break;
                case 12:
                    this.typeOne = 6;
                    this.initSix();
                    break;
                case 13:
                    this.typeOne = 7;
                    this.initSeven();
                    break;
                case 14:
                    this.typeOne = 1;
                    this.initOne();
                    break;
            }
        },
        //初始化数据  this.typeOne = 1
        initOne(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 2
        initTwo(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 3
        initThree(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 4
        initFour(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 5
        initFive(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 6
        initSix(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 7
        initSeven(){
            this.searchData.housing_types = '';  //房屋类型 
        },
        // 添加房源
        addAccount(){
            //清空修改时的数据信息。以免影响addroom页的（修改、添加）状态判断
            sessionStorage.setItem("editroomrow", '');
            this.$router.push({name : 'addroom'});
        },
        //编辑跟进
        showfollowlist(row, event){
            console.log(row);
            this.followsearch.type = 3;   //房客源类型 1房源提醒 2客源提醒   3为房客源普通修改
            this.followsearch.id = '';  //房id或客id
            this.followsearch.cfid = row.f_id;  //房跟进Id 或客跟进id

            this.editfollowdialogVisible = true;

        },
        //格式化   跟进内容
        followFun(row, column, cellValue, index){
            // console.log(row, column, cellValue, index);
            switch (row.f_follow_up_way) {
                case 1:
                    return cellValue + '（去电）'
                case 2:
                    return cellValue + '（来电）'
                case 3:
                    return cellValue + '（看房）'
                case 4:
                    return cellValue + '（勘察）'
                case 5:
                    return cellValue + '（来访）'
                case 6:
                    return cellValue + '（拜访）'
                case 7:
                    return cellValue + '（派单）'
                case 8:
                    return cellValue + '（短信）'
                case 9:
                    return cellValue + '（邮件）'
                case 10:
                    return cellValue + '（信函）'
                case 11:
                    return cellValue + '（申请）'
                case 12:
                    return cellValue + '（修改）'
                case 13:
                    return cellValue + '（保留）'
                case 14:
                    return cellValue + '（其他）'
            }
        },
        //获取部门
        obtainName(data=Object()){
            conf.department.nameOpt(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    this.optionName = res.data.data;
                    // console.log(res); 
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else if(res.data.code == -404){
                        this.optionName = [];
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            }, "qx_room_chakan")
        },
        //获取部门员工
        obtainUsers(val,data=Object()){
            this.userTypes = [];
            this.searchData.source_uid = '';
            data.d_id = val;
            var ownerPermission = window.localStorage.getItem('authories');
            var arr = ownerPermission.split(',');
            if(arr.indexOf(permconfig.qx_room_benren_chakan)  > -1){
                this.userTypes = [{
                    "u_id":window.localStorage.getItem('uid'),
                    "u_username":window.localStorage.getItem("uname")
                }]
                return;
            }
            conf.department.userOpt(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res); 
                    this.userTypes = res.data.data;
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
    },
    created (){
        this.$nextTick(function(){
            this.getList();  // 房源列表
            this.optlist();  //查询下拉数据
            this.obtainName();  //部门下拉数据
        })
    },
}