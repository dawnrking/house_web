import conf from '../config/userconf.js'
import permconfig from "../config/permconfig.js"
import { debug } from 'util';
  export default {
    data() {
        return {
            list:[],
            searchData:{
                c_secondary_area: '',
                c_room: '',
                c_trading_type: '',
                c_buy_status: '',
                c_use_type: '',
                date_type: '',
                start_time: '',
                end_time: '',
                c_room_type: '',
                c_toward: '',
                c_professional_type: '',
                c_price_min: '',
                c_price_max: '',
                c_rent_price_min: '',
                c_rent_price_max: '',
                c_acreage_max: '',
                c_acreage_min: '',
                c_private_guest: '',
                c_store_id: '',
                c_employees_id: '',
                keywords: '',
            },
            current: 1,  //当前页码
            options: [],   //查询下拉数据
            typeOne: 1,  //用途更改    所填资料类型
            trading_type_opt: [  //交易类型下拉数据
                {name: '求购',id: '1'},
                {name: '求租',id: '2'},
            ],
            date_type_opt: [  //日期类型下拉数据
                {name: '首次录入时间',id: '1'},
                {name: '最后跟进时间',id: '2'},
                {name: '委托日期',id: '3'},
            ],
            private_guest: [
                {name: '私客',id: '1'},
                {name: '公客',id: '2'},
            ],
            optionName: [],  //部门下拉数据
            userTypes: [],  // 员工下拉数据
            maininfo: '',  //客源主要信息
            followlist: [],  //跟进列表数据
        }
    },
    methods: {
        //获取客源  查询条件 下拉数据
        optlist(data=Object()){
            conf.resources.searchoptlist(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res);
                    this.options = res.data.data;
                    this.searchData.start_time = res.data.data.lastday;
                    this.searchData.end_time = res.data.data.day;
                    this.searchData.c_store_id = parseInt(window.localStorage.getItem('did'));
                    this.obtainUsers(this.searchData.c_store_id);
                    this.searchData.c_employees_id = parseInt(window.localStorage.getItem('uid'));
                }else if (state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        /* 列表*/
        getList (data=Object()){
            data.page = this.current;
            conf.resources.list(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res.data.data);
                    this.list = res.data.data;
                    this.maininfo = res.data.data.data[0].customerInfo;

                    var ownerPermission = window.localStorage.getItem('authories');
                    var arr = ownerPermission.split(',');
                    //查看私客、看客户不必写跟进  权限
                    if(res.data.data.data[0].c_private_guest == 1){  //私客
                        if(arr.indexOf(permconfig.qx_keyuan_chakansike)  == -1 && res.data.data.data[0].c_employees_id != window.localStorage.getItem('uid')){
                            this.maininfo.info = "【**】 ***********";
                        }
                    }else if(res.data.data.data[0].c_private_guest == 2){  //公客

                        if(arr.indexOf(permconfig.qx_keyuan_kankehububixiegenjin)  == -1 && res.data.data.data[0].c_employees_id != window.localStorage.getItem('uid')){
                            var cache = window.localStorage.getItem("PUBLIC_GUEST" + res.data.data.data[0].c_id);
                            if(!cache){
                                this.maininfo.info = "【**】 ***********";
                            }
                        }
                    }

                    this.followlist = res.data.data.data[0].customerFollow;
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else if(res.data.code == -404){
                        this.list = [];
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
        //添加客源
        addAccount(){
            //清空修改时的数据信息。以免影响addroom页的（修改、添加）状态判断
            sessionStorage.setItem("editresourcesrow", '');
            this.$router.push({name : 'addresource'});
        },
        //修改
        edit(row){
            // console.log(row);
            //修改客源信息时 将数据存储sessionStorage，减少http请求
            sessionStorage.setItem("editresourcesrow", JSON.stringify(row));
            
            this.$router.push({name : 'addresource'});
        },
        //删除
        delect(row,data=Object()){
            console.log(row);
            this.$confirm('确认删除客源 ["'+row.c_numbering+'"] ?', '删除客源', {  
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).then(() => {
                permconfig.qx_keyuan_liebiao_shanchu_function(row, (res)=>{
                    if(!res){
                        return;
                    }
                    data.c_id = row.c_id;
                    conf.resources.reclaim(data,(state,res)=>{
                        if (state == 'success'&&res.data.code == 0){
                            this.getList();
                            this.$message({
                                type: 'success',
                                message: '删除成功!'
                            });
                        }else if(state == 'success'&&res.data.code != 0){
                            if(res.data.code == 401){
                                this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                            }else{
                                this.$message({
                                    type: 'info',
                                    message: res.data.msg
                                });
                            }
                        }
                    })
                })
                
            }).catch(() => {
                this.$message({
                type: 'info',
                message: '已取消删除'
                });          
            });
        },
        //查询
        search(){
            let data = this.searchData;
            this.getList(data);
        },
        //单击某行获取改行的跟进列表、主要信息
        getfollowlist(row, event, column){
            // console.log(row);
            this.maininfo = row.customerInfo;
            //  权限
            var ownerPermission = window.localStorage.getItem('authories');
            var arr = ownerPermission.split(',');
            //查看私客、看客户不必写跟进  权限
            if(row.c_private_guest == 1){  //私客
                if(arr.indexOf(permconfig.qx_keyuan_chakansike)  == -1 && row.c_employees_id != window.localStorage.getItem('uid')){
                    this.maininfo.info = "【**】 ***********";
                }
            }else if(row.c_private_guest == 2){  //公客
                if(arr.indexOf(permconfig.qx_keyuan_kankehububixiegenjin)  == -1 && row.c_employees_id != window.localStorage.getItem('uid')){
                    var cache = window.localStorage.getItem("PUBLIC_GUEST" + row.c_id);
                    if(!cache){
                        this.maininfo.info = "【**】 ***********";
                    }
                }
            }
            this.followlist = row.customerFollow;
        },
        //交易change
        transactChange(val){
            //1 求购、2 求租
            if(val == 1){

                this.searchData.c_rent_price_min = '';
                this.searchData.c_rent_price_max = '';

            }else if(val == 2){

                this.searchData.c_price_min = '';
                this.searchData.c_price_max = '';

            }
        },
        // 用途 change
        typeChange(val){
            console.log(val);
            //3 住宅 、4 商住 、14 其他
            //5 商铺 、6 网点 、10 铺厂 
            //7 写字楼 、9 写厂
            //8 厂房
            //11 仓库
            //12 地皮 
            //13 车位
            switch(val){
                case 3:
                    this.typeOne = 1;
                    this.initOne();
                    break;
                case 4:
                    this.typeOne = 1;
                    this.initOne();
                    break;
                case 5:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 6:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 7:
                    this.typeOne = 3;
                    this.initThree();
                    break;
                case 8:
                    this.typeOne = 4;
                    this.initFour();
                    break;
                case 9:
                    this.typeOne = 3;
                    this.initThree();
                    break;
                case 10:
                    this.typeOne = 2;
                    this.initTwo();
                    break;
                case 11:
                    this.typeOne = 5;
                    this.initFive();
                    break;
                case 12:
                    this.typeOne = 6;
                    this.initSix();
                    break;
                case 13:
                    this.typeOne = 7;
                    this.initSeven();
                    break;
                case 14:
                    this.typeOne = 1;
                    this.initOne();
                    break;
            }
        },
        //初始化数据  this.typeOne = 1
        initOne(){
            this.searchData.c_room_type = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 2
        initTwo(){
            this.searchData.c_room_type = '';  //房屋类型 
        },
        //初始化数据  this.typeOne = 3
        initThree(){
            this.searchData.c_room_type = '';  //房屋类型
        },
        //初始化数据  this.typeOne = 4
        initFour(){
            this.searchData.c_room_type = '';  //房屋类型
        },
        //初始化数据  this.typeOne = 5
        initFive(){
            this.searchData.c_room_type = '';  //房屋类型
        },
        //初始化数据  this.typeOne = 6
        initSix(){
            this.searchData.c_room_type = '';  //房屋类型
        },
        //初始化数据  this.typeOne = 7
        initSeven(){
            this.searchData.c_room_type = '';  //房屋类型 
        },
        //公私
        privateGuest(row, column, cellValue, index){
            switch (cellValue) {
                case 1:
                    return '私客'
                case 2:
                    return '公客'
            }
        },
        // 切换页码
        handleCurrentChange(val) {
            this.current = val;
            this.getList();
        },
        //格式化   跟进内容
        followFun(row, column, cellValue, index){
            // console.log(row, column, cellValue, index);
            switch (row.cf_fllow_type) {
                case 1:
                    return cellValue + '（去电）'
                case 2:
                    return cellValue + '（来电）'
                case 3:
                    return cellValue + '（看房）'
                case 4:
                    return cellValue + '（勘察）'
                case 5:
                    return cellValue + '（来访）'
                case 6:
                    return cellValue + '（拜访）'
                case 7:
                    return cellValue + '（派单）'
                case 8:
                    return cellValue + '（短信）'
                case 9:
                    return cellValue + '（邮件）'
                case 10:
                    return cellValue + '（信函）'
                case 11:
                    return cellValue + '（申请）'
                case 12:
                    return cellValue + '（修改）'
                case 13:
                    return cellValue + '（保留）'
                case 14:
                    return cellValue + '（其他）'
            }
        },
        //获取部门
        obtainName(data=Object()){
            conf.department.nameOpt(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    this.optionName = res.data.data;
                    // console.log(res); 
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else if(res.data.code == -404){
                        this.optionName = [];
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            }, 'qx_keyuan_chakan')
        },
        //获取部门员工
        obtainUsers(val,data=Object()){
            this.userTypes = [];
            this.searchData.c_employees_id = '';
            data.d_id = val;
            var ownerPermission = window.localStorage.getItem('authories');
            var arr = ownerPermission.split(',');
            if(arr.indexOf(permconfig.qx_keyuan_benren_chakan_keyuan)  > -1){
                this.userTypes = [{
                    "u_id":window.localStorage.getItem('uid'),
                    "u_username":window.localStorage.getItem("uname")
                }]
                return;
            }
            conf.department.userOpt(data,(state,res)=>{
                if (state == 'success'&&res.data.code == 0){
                    // console.log(res); 
                    this.userTypes = res.data.data;
                }else if(state == 'success'&&res.data.code != 0){
                    if(res.data.code == 401){
                        this.$router.push({name:'login'});   //登陆态失效，跳转至登录页
                    }else{
                        this.$message({
                            type: 'info',
                            message: res.data.msg
                        });
                    }
                }
            })
        },
    },
    created (){
        this.$nextTick(function(){
            this.getList();  // 房源列表
            this.optlist();  //查询下拉数据
            this.obtainName();  //部门下拉数据
        })
    },
        
  }