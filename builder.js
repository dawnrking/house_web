var electronInstaller = require('electron-winstaller');
var path = require("path");

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: path.join('./release/WenDingFangChan-win32-x64'), //刚才生成打包文件的路径
    outputDirectory: path.join('./dist/step'), //输出路径
    authors: 'ChenJinLong', // 作者名称
    exe: 'WenDingFangChan.exe', //在appDirectory寻找exe的名字
    noMsi: false, 
  });

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));
